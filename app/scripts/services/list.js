(function () {
    'use strict';

    /**
     * @ngdoc service
     * @name mostDisasterApp'
     * @description
     * # list
     * Service in the mostDisasterApp.
     */
    angular.module('mostDisasterApp')
      .factory('list', list);

    list.$inject = ['$http', '$q', 'listUrl'];

    function list($http, $q, listUrl) {
        var service = {
            getArticlelist: getArticlelist,
            getArticlenumber: getArticlenumber
        };
        return service;
        
        function getArticlelist(params) {
            var q = $q.defer();
            $http.post(listUrl + "articlelist", params)
            .success(function(data) {
                q.resolve(data);
            })
            .error(function(err) {
                q.reject(err);
            });
            return q.promise;
        }

        function getArticlenumber(params) {
            var q = $q.defer();
            $http.post(listUrl + "articlenumber", params)
            .success(function(data) {
                q.resolve(data);
            })
            .error(function(err) {
                q.reject(err);
            });
            return q.promise;
        }

    }
    
})();