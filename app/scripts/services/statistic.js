(function () { 
    'use strict';

    /**
     * @ngdoc service
     * @name mostDisasterApp.statistic
     * @description
     * # statistic
     * Service in the mostDisasterApp.
     */
    angular.module('mostDisasterApp')
      .factory('statistic', statistic);

      statistic.$inject = ['$http', '$q', 'statisticUrl'];

      function statistic($http, $q, statisticUrl) {
        var service = {
            getArticle: getArticle ,
            getStatistic: getStatistic ,
            getDonut: getDonut
        };
        return service;

        function getArticle(params) {
            var q = $q.defer();
            $http.post(statisticUrl + "getArticle", params)
            .success(function(data) {
                q.resolve(data);
            })
            .error(function(err) {
                q.reject(err);
            });
            return q.promise;
        }

        function getDonut(params) {
            var q = $q.defer();
            $http.post(statisticUrl + "getDonut", params)
            .success(function(data) {
                q.resolve(data);
            })
            .error(function(err) {
                q.reject(err);
            });
            return q.promise;
        }

        function getStatistic(params) {
            var q = $q.defer();
            $http.post(statisticUrl + "getStatistic", params)
            .success(function(data) {
                q.resolve(data);
            })
            .error(function(err) {
                q.reject(err);
            });
            return q.promise;
        }
      }
})();