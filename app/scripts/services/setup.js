(function () {
    'use strict';

    /**
     * @ngdoc service
     * @name mostDisasterApp' 
     * @description
     * # setup
     * Service in the mostDisasterApp.
     */
    angular.module('mostDisasterApp')
      .factory('setup', setup);

      setup.$inject = ['$http', '$q', 'facebookUrl'];

      function setup($http, $q, facebookUrl) {
        var service = {
            fbAccessToken: fbAccessToken,
            fbAccessTokenInfo: fbAccessTokenInfo,
            addToPageCrawlerList: addToPageCrawlerList,
            addToGroupCrawlerList: addToGroupCrawlerList,
            getCrawlerList: getCrawlerList,
            stopCrawl: stopCrawl,
            restartCrawl: restartCrawl
        };
        return service;

        function fbAccessToken(params) {
            var q = $q.defer();
            $http.post(facebookUrl + "AccessTokenUpload", params)
            .success(function(data) {
                q.resolve(data);
            })
            .error(function(err) {
                q.reject(err);
            });
            return q.promise;
        }

        function fbAccessTokenInfo() {
            var q = $q.defer();
            $http.get(facebookUrl + "CurrentAccessTokenInfo")
            .success(function(data) {
                q.resolve(data);
            })
            .error(function(err) {
                q.reject(err);
            });
            return q.promise;
        }

        function addToPageCrawlerList(params) {
            var q = $q.defer();
            $http.post(facebookUrl + "AddToPageCrawlerList", params)
            .success(function(data) {
                q.resolve(data);
            })
            .error(function(err) {
                q.reject(err);
            });
            return q.promise;
        }

        function addToGroupCrawlerList(params) {
            var q = $q.defer();
            $http.post(facebookUrl + "AddToGroupCrawlerList", params)
            .success(function(data) {
                q.resolve(data);
            })
            .error(function(err) {
                q.reject(err);
            });
            return q.promise;
        }

        function getCrawlerList(params) {
            var q = $q.defer();
            $http.get(facebookUrl + "CrawlerList", params)
            .success(function(data) {
                q.resolve(data);
            })
            .error(function(err) {
                q.reject(err);
            });
            return q.promise;
        }

        function stopCrawl(params) {
            var q = $q.defer();
            if (params.type === "Page") {
                $http.put(facebookUrl + "StopPageCrawl/" + params.facebookId)
                .success(function(data) {
                    q.resolve(data);
                })
                .error(function(err) {
                    q.reject(err);
                });
            } else {
                $http.put(facebookUrl + "StopGroupCrawl/" + params.facebookId)
                .success(function(data) {
                    q.resolve(data);
                })
                .error(function(err) {
                    q.reject(err);
                });
            }
            return q.promise;
        }

        function restartCrawl(params) {
            var q = $q.defer();
            if (params.type === "Page") {
                $http.put(facebookUrl + "RestartPageCrawl/" + params.facebookId)
                .success(function(data) {
                    q.resolve(data);
                })
                .error(function(err) {
                    q.reject(err);
                });
            } else {
                $http.put(facebookUrl + "RestartGroupCrawl/" + params.facebookId)
                .success(function(data) {
                    q.resolve(data);
                })
                .error(function(err) {
                    q.reject(err);
                });
            }
            return q.promise;
        }
    }
})();