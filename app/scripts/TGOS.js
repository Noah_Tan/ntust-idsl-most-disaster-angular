    var pTGMarker; //Marker物件
    var markerPoint; //Marker位置
    var markerTitle; //Marker標題
    var markerImg; //Marker圖片
    var trans;
    var pMap;
    var infodata;
    var infodata_all;
    var InfoWindowOptions;
    var messageBox;
    var messageBoxArray = new Array();
    //var point;
    var Markers = new Array();
    var color = [
        "/images/tag/tagR.png",//r
        "/images/tag/tagG.png",//g
        "/images/tag/tagB.png",//b
        "/images/tag/icon.png",//heat
        "/images/tag/focus.png"//focus
    ];
  
    window.onload = InitWnd();
    var script = document.createElement('script');
    script.src = 'bower_components/jquery/dist/jquery.js';
    script.type = 'text/javascript';
    document.getElementsByTagName('head')[0].appendChild(script);  
    function InitWnd() {
        var trans = new TGOS.TGTransformation();
        var pOMap = document.getElementById("TGMap");//ini
        InfoWindowOptions = {
            maxWidth: 1000,
            pixelOffset: { x: 0, y: 0 },
            zIndex: 0
        };

        pMap = new TGOS.TGOnlineMap(pOMap, TGOS.TGCoordSys.EPSG3826); //ini//宣告TGOnlineMap地圖物件並設定坐標系統
        pMap.setCenter (new TGOS.TGPoint(trax(121.525356,25.020356),tray(121.525356,25.020356)));
        pMap.setZoom(4);
        getpoint();
    }

    function setCenter(lat,lon,i,simple_mode){
        console.log(i)
        //console.log(lat+" "+lon);
        //console.log("center" + trax(lon,lat)+" "+tray(lon,lat) );
        pMap.setCenter(new TGOS.TGPoint( trax(lon,lat),tray(lon,lat) ));
        pMap.setZoom(4);
        //console.log("tag"+pMap.getCenter().x + " , " + pMap.getCenter().y);
        
        if(simple_mode){
                var ini_Img = new TGOS.TGImage(color[3], new TGOS.TGSize(25, 25), new TGOS.TGPoint(0, 0), new TGOS.TGPoint(12.5, 12.5));//heat
                for(var j in Markers)
                    Markers[j].setIcon(ini_Img);
                var fcousImg = new TGOS.TGImage(color[4], new TGOS.TGSize(25, 25), new TGOS.TGPoint(0, 0), new TGOS.TGPoint(12.5, 12.5));//heat
                Markers[i].setIcon(fcousImg);
        }
    }

    //x=>經度,y=>緯度
    function trax(x,y){
        trans = new TGOS.TGTransformation();
        trans.wgs84totwd97(x ,y);                        
        return trans.transResult.x;
    }//translate 3826 =>3857 經度

    function tray(x,y){
        trans = new TGOS.TGTransformation();
        trans.wgs84totwd97(x,y);
        return trans.transResult.y;
    }//translate 3826 =>3857 緯度

    function tag(i,data,simple_mode){//
        var index = i;
        var x,y;
        x = trax(data.y,data.x);
        y = tray(data.y,data.x);  
        //console.log(data.y+ ' ' +data.x +' '+x+' '+y);
        markerPoint = new TGOS.TGPoint(x, y);
        if(simple_mode){
            markerImg = new TGOS.TGImage(color[3], new TGOS.TGSize(25, 25), new TGOS.TGPoint(0, 0), new TGOS.TGPoint(12.5, 12.5));//heat
        }else
        switch (data.tag){
            case 0://red
            markerImg = new TGOS.TGImage(color[0], new TGOS.TGSize(19, 32), new TGOS.TGPoint(0, 0), new TGOS.TGPoint(9.5, 32));//green
            break;
            case 1://green
            markerImg = new TGOS.TGImage(color[1], new TGOS.TGSize(19, 32), new TGOS.TGPoint(0, 0), new TGOS.TGPoint(9.5, 32));//blue
            break;
            default: //blue
            markerImg = new TGOS.TGImage(color[2], new TGOS.TGSize(19, 32), new TGOS.TGPoint(0, 0), new TGOS.TGPoint(9.5, 32));//red
            break;
        }

        pTGMarker = new TGOS.TGMarker(pMap, markerPoint, data.address, markerImg);
        Markers.push(pTGMarker);
        TGOS.TGEvent.addListener(pTGMarker, 'click', function () {
            if(simple_mode){
                var ini_Img = new TGOS.TGImage(color[3], new TGOS.TGSize(25, 25), new TGOS.TGPoint(0, 0), new TGOS.TGPoint(12.5, 12.5));//heat
                for(var j in Markers)
                    Markers[j].setIcon(ini_Img);
                var fcousImg = new TGOS.TGImage(color[4], new TGOS.TGSize(25, 25), new TGOS.TGPoint(0, 0), new TGOS.TGPoint(12.5, 12.5));//heat
                Markers[i].setIcon(fcousImg);
            }
            //Markers[i] = fcousMkr;
            /*closeMessage();
            createMessage(x, y,
                "<div style='margin: 0px auto; overflow:hidden; overflow-y:a; max-width:95%; max-height:400; font-family:consolas,微軟正黑體;  '>" + 
                "<h1>台北火山爆發瞜!!!!快逃阿</h1>" + 
                "<h2>上傳時間:2015-2-27 08:00</h2>" +
                "<img src='https://googledrive.com/host/0B_Voet-2b-tGZXNkcmhtQzhRNHM/' style='max-height:95%; max-width:95%; margin:1%'>" +
                "<h4>" + data.text + "</h4>" + 
                "<button type='button' onclick='change(" + (index+1) + ", 1);' class='btn btn-success'>證實</button>" +
                "<span> </span>"+ 
                "<button type='button' onclick='change(" + (index+1) + ", 0);' class='btn btn-warning'>駁回</button>" + 
                "<span> </span>"+ 
                "<button type='button' onclick='change(" + (index+1) + ", 2);' class='btn btn-danger'>無法確認</button>" + 
                "<span> </span>"+ 
                "</div>"
            );*/
        document.getElementById("article").innerHTML ="<thead><tr><th>" +
        "<div class='btn-group' role='group' >" +
        "<button type='button' class='btn btn-default' onclick='marker_summary()'>顯示災害熱點</button>" +
        "<button type='button' class='btn btn-default' onclick='marker_all()'>顯示全部事件</button></div>" +
        "</th></tr></thead>";
        if(!simple_mode){
        console.log(1)
                document.getElementById("article").innerHTML =document.getElementById("article").innerHTML+ //"<script src='scripts/controllers/map.js'></script>" +
                "<thead><tr><th><div>" +
                "<h4>"+ infodata_all[i].address + "</h4>" +
                "<h5>"+ infodata_all[i].time +"</h5>" +
                "<h5>"+ infodata_all[i].content +"</h5>" +
                "<img src='https://s3.yimg.com/bt/api/res/1.2/hw_lwfSZJ_f6iQNxjRau_g--/YXBwaWQ9eW5ld3NfbGVnbztxPTg1O3c9NjAw/http://media.zenfs.com/zh_hant_tw/News/stormmedia/20160602-044454_U1841_M162251_e939.jpgitokSYjntWfm' width='80%' ng-click='map_total.test()'></br></br>" +
                "<button type='button' onclick='setCenter(" + parseInt(infodata_all[i].x) + ","+parseInt(infodata_all[i].y)+","+i+"," +false +");' class='btn btn-info'>地點</button>" +
                "<span> </span><button type='button' onclick='change(" + parseInt(infodata_all[i].id) + ", 1);' class='btn btn-success'>證實</button>" +
                "<span> </span><button type='button' onclick='change(" + parseInt(infodata_all[i].id) + ", 0);' class='btn btn-warning'>駁回</button>" + 
                "<span> </span><button type='button' onclick='change(" + parseInt(infodata_all[i].id) + ", 2);' class='btn btn-danger'>無法確認</button>" + 
                "<span> </span><button type='button'  id='launchStyledModal' class='btn' onclick='dialog(\""+infodata_all[i].content+"\","+infodata_all[i].x+","+infodata_all[i].y+")'>在確認</button>" + 
                "<span> </span></br></div></th></tr></thead><hr>";
                
        }else{
            console.log(2);
            for(var j in infodata[i].group){
                document.getElementById("article").innerHTML =document.getElementById("article").innerHTML+ //"<script src='scripts/controllers/map.js'></script>" +
                "<thead><tr><th><div>" +
                "<h4>"+ infodata[i].group[j].address + "</h4>" +
                "<h5>"+ infodata[i].group[j].time +"</h5>" +
                "<h5>"+ infodata[i].group[j].content +"</h5>" +
                "<img src='https://s3.yimg.com/bt/api/res/1.2/hw_lwfSZJ_f6iQNxjRau_g--/YXBwaWQ9eW5ld3NfbGVnbztxPTg1O3c9NjAw/http://media.zenfs.com/zh_hant_tw/News/stormmedia/20160602-044454_U1841_M162251_e939.jpgitokSYjntWfm' width='80%' ng-click='map_total.test()'></br></br>" +
                "<button type='button' onclick='setCenter(" + parseInt(infodata[i].group[j].x) + ","+parseInt(infodata[i].group[j].y)+","+i+"," +true +");' class='btn btn-info'>地點</button>" +
                "<span> </span><button type='button' onclick='change(" + parseInt(infodata[i].group[j].id) + ", 1);' class='btn btn-success'>證實</button>" +
                "<span> </span><button type='button' onclick='change(" + parseInt(infodata[i].group[j].id) + ", 0);' class='btn btn-warning'>駁回</button>" + 
                "<span> </span><button type='button' onclick='change(" + parseInt(infodata[i].group[j].id) + ", 2, map.token);' class='btn btn-danger'>無法確認</button>" + 
                "<span> </span><button type='button'  id='launchStyledModal' class='btn' onclick='dialog(\""+infodata[i].group[j].content+"\","+infodata[i].group[j].x+","+infodata[i].group[j].y+")'>在確認</button>" + 
                "<span> </span></br></div></th></tr></thead><hr>";
                
            }
            
        }
        //console.log(infodata);
        //console.log(pMap.getCenter().x + " , " + pMap.getCenter().y);

        });//info
        

       }//print different color tag
    //add CENTER_CHANGE listener and updat

    function RmvMarker(i) {
        change(i+1,0);
    }        

    //create info windows
    function createMessage(x,y,text){
        var InfoWindowOptions = {
            maxWidth: 600,
            pixelOffset: { x: 0, y: 0 },
            zIndex: 0
        };
        messageBox = new TGOS.TGInfoWindow(text, new TGOS.TGPoint(x, y), InfoWindowOptions);
        messageBox.open(pMap);
        messageBoxArray.push(messageBox);
    }
    function closeMessage() {
        for (var i = 0; i < messageBoxArray.length; i++) {
        messageBoxArray[i].close(); //關閉訊息視窗
        }
    }

    function massagechange(x,y,text){//change infowindows
        var newPosition = new TGOS.TGPoint(x, y);
        messageBox.setPosition(newPosition);
        messageBox.setContent(text);
    }

    function getpoint(){//call marker
        var point
        $.ajax({
            method: 'post',
            url: 'http://140.118.109.149:3002/map/fuzzy',
            success: function(data){
                if(data){
                    point = data;
                    infodata = point;
                    infodata_all = [];
                    for(i = 0;i<infodata.length;i++){
                        for(j in infodata[i].group){
                            infodata_all.push(infodata[i].group[j]);
                        }
                    }   
                    infodata_all.sort(function(a,b){
                        return a["id"] > b["id"] ? 1 : (a["id"] == b["id"] ? 0 : -1);}
                    );
                }else{
                    console.log("fail");
                }
                    //console.log(infodata_all);
                    marker_summary(); 
            }
        });
    }
  
    function marker_ini(){
        if(Markers.length>0)
            for (i=0; i < Markers.length; i++) {
                    Markers[i].setMap(null);
            }               
        Markers = [];
    }

    function marker_all(){
        //console.log(data);
        marker_ini();
        var i,j,x,y,num=0;
        for(i = 0;i<infodata.length;i++){
                tag(i ,infodata_all[i],false);
        }
        zoomin();
    }   

    function marker_summary(){
        //console.log(data);
        marker_ini();
        var i,x,y;
        for(i = 0;i<infodata.length;i++){
            //console.log()
            x = trax(infodata[i].group[0].y,infodata[i].group[0].x);
            y = tray(infodata[i].group[0].y,infodata[i].group[0].x);
            //console.log(x+' '+y+' '+data[i].group[0].tag);
            tag(i,infodata[i].group[0],true);
        }
        zoomout();
    }

    function zoomin(){
        console.log(0)
        //console.log(infodata);
        document.getElementById("article").innerHTML ="<thead><tr><th>" +
        "<div class='btn-group' role='group'>" +
        "<button type='button' class='btn btn-default' onclick='marker_summary()'>顯示災害熱點</button>" +
        "<button type='button' class='btn btn-default' onclick='marker_all()'>顯示全部事件</button></div>" +
        "</th></tr></thead>";
        for(var i in infodata_all){
            
            document.getElementById("article").innerHTML =document.getElementById("article").innerHTML+ //"<script src='scripts/controllers/map.js'></script>" +
            "<thead><tr><th><div>" +
            "<h4>"+ infodata_all[i].address +"</h4>" +
            "<h5>"+ infodata_all[i].time +"</h5>" +
            "<h5>"+ infodata_all[i].content +"</h5>" +
            "<img src='https://s3.yimg.com/bt/api/res/1.2/hw_lwfSZJ_f6iQNxjRau_g--/YXBwaWQ9eW5ld3NfbGVnbztxPTg1O3c9NjAw/http://media.zenfs.com/zh_hant_tw/News/stormmedia/20160602-044454_U1841_M162251_e939.jpgitokSYjntWfm' width='80%' ng-click='map_total.test()'></br></br>" +
            "<button type='button' onclick='setCenter(" + parseInt(infodata_all[i].x) + ","+parseInt(infodata_all[i].y)+","+i+"," +false +");' class='btn btn-info'>地點</button>" +
            "<span> </span><button type='button' onclick='change(" + parseInt(infodata_all[i].id) + ", 1);' class='btn btn-success'>證實</button>" +
            "<span> </span><button type='button' onclick='change(" + parseInt(infodata_all[i].id) + ", 0);' class='btn btn-warning'>駁回</button>" + 
            "<span> </span><button type='button' onclick='change(" + parseInt(infodata_all[i].id) + ", 2);' class='btn btn-danger'>無法確認</button>" + 
            "<span> </span><button type='button' id='launchStyledModal' class='btn' onclick='dialog(\""+infodata_all[i].content+"\","+infodata_all[i].x+","+infodata_all[i].y+")'>在確認</button>" + 
            "</br></div></th></tr></thead><hr>";
        }
    }

    function zoomout(){
        //console.log(parseInt(data[0].group[0].id));
        document.getElementById("article").innerHTML ="<thead><tr><th>" +
        "<div class='btn-group' role='group'>" +
        "<button type='button' class='btn btn-default' onclick='marker_summary()'>顯示災害熱點</button>" +
        "<button type='button' class='btn btn-default' onclick='marker_all()'>顯示全部事件</button></div>" +
        "</th></tr></thead>";
        for(var i in infodata){
            document.getElementById("article").innerHTML =document.getElementById("article").innerHTML+ //"<script src='scripts/controllers/map.js'></script>" +
            "<thead><tr><th><div>" +
            "<h4>"+ infodata[i].group[0].address +"</h4>" +
            "<h5>"+ infodata[i].group[0].time +"</h5>" +
            "<h5>"+ infodata[i].group[0].content +"</h5>" +
            "<button type='button' onclick='setCenter(" + parseInt(infodata[i].group[0].x) + ","+parseInt(infodata[i].group[0].y)+","+i+"," +true +");' class='btn btn-info'>地點</button>" +
            "<span> </span></br></div></th></tr></thead><hr>";
        }
    }

    function change(num,tag){
        //alert(num+' '+tag);
        Markers[num-1].setIcon(new TGOS.TGImage(color[tag], new TGOS.TGSize(19, 32), new TGOS.TGPoint(0, 0), new TGOS.TGPoint(9.5, 32)));
        $.ajax({
            type: 'post',
            url: 'http://140.118.109.149:3002/map/change',
            crossDomain: true,
            data:JSON.stringify({tag:tag,num:num}),
            headers: {'Content-Type': 'application/json'},
            dataType: "json",
            success: function (err) {
                if (err) {
                    console.log(err)
                } else {
                    console.log('success');
                }
            }
        })
        //argument like markerImg
    }
function dialog(content,x,y){
    // Get the dialogs.
    var styledModal = document.getElementById('styledModal');
            styledModal.showModal();
            styledModal.innerHTML=
            "<p>推播任務</p>"+
			"<div class=\"input-group\">"+
				"<span class=\"input-group-addon\" id=\"basic-addon1\">經度：</span>"+
				"<input type=\"text\" class=\"form-control\" placeholder="+y+" aria-describedby=\"basic-addon1\" disabled=\"disabled\">"+
				"<span class=\"input-group-addon\" id=\"basic-addon1\">緯度：</span>"+
				"<input type=\"text\" class=\"form-control\" placeholder="+x+" aria-describedby=\"basic-addon1\" disabled=\"disabled\">"+
			"</div></br>"+
			"<div class=\"input-group\">"+
				"<span class=\"input-group-addon\" id=\"basic-addon1\">推播<br>訊息</span>"+
				"<textarea class=\"form-control\" rows=\"5\" id=\"comment\">"+content+"</textarea>"+
			"</div><!-- /input-group --></br>"+
			"<button class=\"closedialog btn-success btn\" onclick='push_notify(\""+content+"\","+y+","+x+")'>推播任務</button><span> </span>"+
			"<button class=\"closedialog btn-danger btn\" onclick=\"close_dialog()\">取消</button>";

            var closeBtns = document.querySelectorAll('.closedialog');
    
            // Setup Event Listeners
            for (var j = 0; j < closeBtns.length; j++) {
                console.log(j)
                closeBtns[j].addEventListener('click', function() {
                    document.querySelectorAll('button#launchStyledModal').innerHTML="success";
                    styledModal.close();
                });
            }

}

function push_notify(message,lat,lng){
    $.ajax({
        type: 'POST',
        url: 'http://140.118.109.149:3002/map/push',//?lat='+parseInt(lat)+'&lng='+parseInt(lng)+'&message='+message,
        crossDomain: true,
        data:JSON.stringify({lat:parseInt(lat),lng:parseInt(lng),message:message}),
        headers: {'Content-Type': 'application/json; charset=utf-8'},
        dataType: "json",
        success: function (err) {
            if (err) {
                console.log(err)
            } else {
                console.log('success');
            }
        }
    });
    close_dialog();
}

function close_dialog(){
    var styledModal = document.getElementById('styledModal');
    styledModal.close();
}
