    var pTGMarker; //Marker物件
    var markerPoint; //Marker位置
    var markerTitle; //Marker標題
    var markerImg; //Marker圖片
    var trans;
    var pMap;
    var infodata;
    var InfoWindowOptions;
    var messageBox;
    var messageBoxArray = new Array();
    //var point;
    var Markers = new Array();
    var color = [
        "/images/tag/tagR.png",//r
        "/images/tag/tagG.png",//g
        "/images/tag/tagB.png",//b
        "/images/tag/icon.png",//heat
        "/images/tag/focus.png"//focus
    ];

  
    window.onload = InitWnd();
    function InitWnd() {
        var trans = new TGOS.TGTransformation();
        var pOMap = document.getElementById("TGMap");//ini
        InfoWindowOptions = {
            maxWidth: 1000,
            pixelOffset: { x: 0, y: 0 },
            zIndex: 0
        };

        pMap = new TGOS.TGOnlineMap(pOMap, TGOS.TGCoordSys.EPSG3826); //ini//宣告TGOnlineMap地圖物件並設定坐標系統
        pMap.setCenter (new TGOS.TGPoint(trax(121.525356,25.020356),tray(121.525356,25.020356)));
        pMap.setZoom(4);
        getpoint();
    }

    function setCenter(lat,lon,i){
        
        //console.log(lat+" "+lon);
        //console.log("center" + trax(lon,lat)+" "+tray(lon,lat) );
        pMap.setCenter(new TGOS.TGPoint( trax(lon,lat),tray(lon,lat) ));
        pMap.setZoom(4);
        //console.log("tag"+pMap.getCenter().x + " , " + pMap.getCenter().y);
        

                var ini_Img = new TGOS.TGImage(color[1], new TGOS.TGSize(19, 32), new TGOS.TGPoint(0, 0), new TGOS.TGPoint(9.5, 32));//green
                for(var j in Markers)
                    Markers[j].setIcon(ini_Img);
                var fcousImg = new TGOS.TGImage(color[0], new TGOS.TGSize(19, 32), new TGOS.TGPoint(0, 0), new TGOS.TGPoint(9.5, 32));//red
                Markers[i].setIcon(fcousImg);
    }

    //x=>經度,y=>緯度
    function trax(x,y){
        trans = new TGOS.TGTransformation();
        trans.wgs84totwd97(x ,y);                        
        return trans.transResult.x;
    }//translate 3826 =>3857 經度

    function tray(x,y){
        trans = new TGOS.TGTransformation();
        trans.wgs84totwd97(x,y);
        return trans.transResult.y;
    }//translate 3826 =>3857 緯度

    function tag(i,data){//
        var index = i;
        var x,y;
        x = trax(data.y,data.x);
        y = tray(data.y,data.x);  
        //console.log(data.y+ ' ' +data.x +' '+x+' '+y);
        markerPoint = new TGOS.TGPoint(x, y);
        
        switch (data.tag){
            case 0://red
            markerImg = new TGOS.TGImage(color[0], new TGOS.TGSize(19, 32), new TGOS.TGPoint(0, 0), new TGOS.TGPoint(9.5, 32));//red
            break;
            case 1://green
            markerImg = new TGOS.TGImage(color[1], new TGOS.TGSize(19, 32), new TGOS.TGPoint(0, 0), new TGOS.TGPoint(9.5, 32));//green
            break;
            default: //blue
            markerImg = new TGOS.TGImage(color[2], new TGOS.TGSize(19, 32), new TGOS.TGPoint(0, 0), new TGOS.TGPoint(9.5, 32));//blue
            break;
        }

        pTGMarker = new TGOS.TGMarker(pMap, markerPoint, data.address, markerImg);
        Markers.push(pTGMarker);
        TGOS.TGEvent.addListener(pTGMarker, 'click', function () {
                var ini_Img = new TGOS.TGImage(color[0], new TGOS.TGSize(25, 25), new TGOS.TGPoint(0, 0), new TGOS.TGPoint(12.5, 12.5));//heat
                for(var j in Markers)
                    Markers[j].setIcon(ini_Img);
                var fcousImg = new TGOS.TGImage(color[1], new TGOS.TGSize(25, 25), new TGOS.TGPoint(0, 0), new TGOS.TGPoint(12.5, 12.5));//heat
                Markers[i].setIcon(fcousImg);

            
        document.getElementById("article").innerHTML ="<thead><tr><th>" +
        "<div class='btn-group' role='group'>" +
        "<button type='button' class='btn btn-default' onclick='marker_all()'>顯示全部事件</button></div>" +
        "</th></tr></thead>";
        
                document.getElementById("article").innerHTML =document.getElementById("article").innerHTML+ //"<script src='scripts/controllers/map.js'></script>" +
                "<thead><tr><th><div>" +
                "<h4>"+ infodata[i].address + "</h4>" +
                "<h5>"+ new Date(infodata[i].time).toString() +"</h5>" +
                "<h5>"+ infodata[i].content +"</h5>" +
                "<button type='button' onclick='setCenter(" + parseInt(infodata[i].x) + ","+parseInt(infodata[i].y)+","+i+"," +false +");' class='btn btn-info'>地點</button>" +
                "<span> </span></br></div></th></tr></thead><hr>";
         
        //console.log(pMap.getCenter().x + " , " + pMap.getCenter().y);

        });//info
       }//print different color tag
    //add CENTER_CHANGE listener and updat

    //create info windows

    function getpoint(){//call marker
        var point
        $.ajax({
            method: 'post',
            url: 'http://140.118.109.149:3002/map/total',
            success: function(data){
                if(data){
                    point = data;
                    infodata = point;
                }else{
                    console.log("fail");
                }
                    //console.log(infodata);
                    marker_all(); 
            }
        });
    }
   
    function marker_all(){
        //console.log(data);
        marker_ini();
        var i,j,x,y;
        for(i = 0;i<infodata.length;i++){
            tag(i,infodata[i],false);
        }
        zoomin();
    }

    function zoomin(){
        //console.log(infodata);
        document.getElementById("article").innerHTML ="<thead><tr><th>" +
        "<div class='btn-group' role='group'>" +
        "</th></tr></thead>";

        for(var i in infodata){
            
            document.getElementById("article").innerHTML =document.getElementById("article").innerHTML+ //"<script src='scripts/controllers/map.js'></script>" +
            "<thead><tr><th><div>" +
            "<h4>"+ infodata[i].address +"</h4>" +
            "<h5>"+ new Date(infodata[i].time).toString() +"</h5>" +
            "<h5>"+ infodata[i].content +"</h5>" +
            "<button type='button' onclick='setCenter(" + parseInt(infodata[i].x) + ","+parseInt(infodata[i].y)+","+i+"," +false +");' class='btn btn-info'>地點</button>" +
            "</br></div></th></tr></thead><hr>";
        }
    }

    function marker_ini(){
                  if(Markers.length>0)
                  for (i=0; i < Markers.length; i++) {
                          Markers[i].setMap(null);
                  }               
                  Markers = [];
    }
