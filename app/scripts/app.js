'use strict';

/**
 * @ngdoc overview
 * @name mostDisasterApp
 * @description
 * # mostDisasterApp
 *
 * Main module of the application.
 */
angular
  .module('mostDisasterApp', [
    'ngAnimate',
    'ngCookies',
    'ngDialog',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ngStorage',
    'ui.router',
    'chart.js',
    'ngFacebook',
    'ui.bootstrap',
    'angularMoment'
  ])
  //Set up your backend service ip
  .constant('authUrl', "http://140.118.109.149:3002/auth/")
  .constant('backendUrl', "http://140.118.109.149:3002/facebook/")
  .constant('backendUrl2', "http://140.118.109.149:3002/api/")
  .constant('mapUrl', "http://140.118.109.149:3002/map/")
  .constant('facebookUrl', "http://140.118.109.149:3002/facebook/")
  .constant('listUrl', "http://140.118.109.149:3002/list/")
  .constant('opinionUrl', "http://140.118.109.149:3002/opinion/")
  .constant('statisticUrl', "http://140.118.109.149:3002/statistic/")
  .config(function ($facebookProvider, $stateProvider, $urlRouterProvider) {
    //Configure Facebook Application ID
    $facebookProvider.setAppId('880131062107839');

    $urlRouterProvider.otherwise("/");
    $stateProvider
    .state('statistic', {
      url: "/",
      templateUrl: "views/statistic.html",
      controller: 'StatisticCtrl',
      controllerAs: 'statistic'
    })
    .state('map_total', {
      url: "/map_total",
      templateUrl: "views/map_total.html",
      controller: 'Map_totalCtrl',
      controllerAs: 'map_total'
      })    
    .state('map', {
      url: "/map",
      templateUrl: "views/map.html",
      controller: 'MapCtrl',
      controllerAs: 'map'
      })    
    .state('list', {
      url: "/list",
      templateUrl: "views/list.html",
      controller: 'ListCtrl',
      controllerAs: 'list'
    })
    .state('opinion', {
      url: "/opinion",
      templateUrl: "views/opinion.html",
      controller: 'OpinionCtrl',
      controllerAs: 'opinion'
    })
    .state('setup', {
      url: "/setup",
      templateUrl: "views/setup.html",
      controller: 'SetupCtrl',
      controllerAs: 'setup'
    })
    .state('login', {
      url: "/login",
      templateUrl: "views/login.html",
      controller: 'AuthCtrl',
      controllerAs: 'auth'
    })
    .state('signup', {
      url: "/signup",
      templateUrl: "views/signup.html",
      controller: 'AuthCtrl',
      controllerAs: 'auth'
    })
    .state('logout', {
      url: "/logout",
      templateUrl: "views/signup.html",
      controller: function($rootScope, $state, $localStorage) {
        delete $rootScope.user;
        delete $localStorage.user;
        $state.go('login');
      }
    });
  })
  .run( function() {
  // Cut and paste the "Load the SDK" code from the facebook javascript sdk page.

  // Load the facebook SDK asynchronously
    (function(d, s, id){
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {return;}
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
});
