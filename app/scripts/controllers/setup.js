(function () {
    'use strict';

    /**
     * @ngdoc function
     * @name mostDisasterApp.controller:SetupCtrl
     * @description
     * # SetupCtrl
     * Controller of the mostDisasterApp
     */
    angular.module('mostDisasterApp')
      .controller('SetupCtrl', Setup);

      Setup.$inject = ['$rootScope', '$localStorage', '$state', '$q', '$facebook', 'moment', 'setup'];

      function Setup($rootScope, $localStorage, $state, $q, $facebook, moment, setup) {
        var self = this;

        self.fbLogin = fbLogin; // 登入 FB 來提供 Access Token
        self.checkFbLink = checkFbLink; // 檢查所輸入 Facebook 連結
        self.addToCrawlerList = addToCrawlerList; // 加入到爬蟲清單
        self.previousGroup = previousGroup; // 選擇社團：前一個社團
        self.nextGroup = nextGroup; // 選擇社團：下一個社團
        self.changeToPageList = changeToPageList; // 爬蟲清單切換：切換到粉絲專頁
        self.changeToGroupList = changeToGroupList; // 爬蟲清單切換：切換到社團
        self.stopCrawl = stopCrawl; // 停止該社團或粉絲專頁爬蟲
        self.restartCrawl = restartCrawl; // 開始該社團或粉絲專頁爬蟲

        $rootScope.user = $localStorage.user;
        $rootScope.$state = $state;
        var checkGroupAmount, checkGroupIndex;
        var checkGroupList = {};
        var crawlerList= {};
        var token = {};
        token.token = $localStorage.token;
        self.fbPlaceholder = "請直接貼上 URL 連結 Example: https://www.facebook.com/…";
        self.notFbLink = true;
        self.groupPanel = true;
        self.checkResult = {};
        self.pageSize = 18;
        self.currentPage = 1;

        activate();

        function activate() {
            if (!$localStorage.user) { $state.go('login'); } // 若未登入導向到登入頁面
            getAccessTokenInfo(); // 取得目前使用的 Access Token 資訊
            getCrawlerList(); // 取得爬蟲清單
        }

        function getAccessTokenInfo() {
            setup.fbAccessTokenInfo()
            .then(function(accessTokenInfo) {
                self.currentToken = accessTokenInfo.message.accessToken;
                self.tokenDeadline = moment(accessTokenInfo.message.expiresDate * 1000).format("MMMM D YYYY, HH:mm");
            })
            .catch(function(err){ console.log(err); });
        }

        function getCrawlerList() {
            setup.getCrawlerList(token)
            .then(function(lists) {
                crawlerList = lists;
                crawlerList.message.pages.forEach(function(page) {
                    if (page.updatedTime === -1) { page.updatedTime = "尚未更新"; }
                    else { page.updatedTime = moment(page.updatedTime).format("MMMM D YYYY, HH:mm"); }
                    switch (page.status) {
                        case -1:
                            page.status = "停止中";
                            break;
                        case 1:
                            page.status = "爬蟲中";
                            break;
                    }
                });
                crawlerList.message.groups.forEach(function(group) {
                    if (group.updatedTime === -1) { group.updatedTime = "尚未更新"; }
                    else { group.updatedTime = moment(group.updatedTime).format("MMMM D YYYY, HH:mm"); }
                    switch (group.status) {
                        case -1:
                            group.status = "停止中";
                            break;
                        case 1:
                            group.status = "爬蟲中";
                            break;
                    }
                });
                self.items = crawlerList.message.pages;
                self.totalItems = parseInt(crawlerList.message.pages.length);
                self.atPageList = true;        
            })
            .catch(function(err){ console.log(err); });
        }

        function fbLogin() {
            $facebook.login().then(function(data) {
                setup.fbAccessToken(data.authResponse).then(function() {
                    self.tokenUpload = true;
                    self.btnTokenUpload = "成功";
                    getAccessTokenInfo();
                })
                .catch(function() { 
                    self.tokenUpload = false;
                    self.btnTokenUpload = "失敗";
                });
            });
        }

        function checkFbLink() {
            self.checkType = "";
            self.multipleChoice = false;
            self.uploadStatus = "";
            self.btnUpload = "";
            if(self.addPageGroup.split("/")[2] !== "www.facebook.com") {
                self.notFbLink = true;
                self.groupPanel = true;
            } else {
                if (self.addPageGroup.split("/")[3] === "groups") {
                    if (isNaN(self.addPageGroup.split("/")[4])) {
                        self.multipleChoice = false;
                        self.checkType = "社團";
                        $facebook.api("/search?q=" + self.addPageGroup.split("/")[4] + "&type=group").then(function(groupInfo) {                            
                            if (groupInfo.data.length !== 1) { self.multipleChoice = true; }
                            self.groupPanel = false;
                            self.firstGroup = true;
                            checkGroupList = groupInfo;
                            checkGroupAmount = groupInfo.data.length;
                            checkGroupIndex = 0;
                            self.checkResult = checkGroupList.data[checkGroupIndex];
                            checkGroupIsExist(self.checkResult.id);
                        })
                        .catch(function(){ self.notFbLink = true; });
                    } else {
                        $facebook.api("/"+ self.addPageGroup.split("/")[4]).then(function(groupInfo) {
                            self.groupPanel = false;
                            self.checkResult = groupInfo;
                            self.checkType = "社團";
                            checkGroupIsExist(self.checkResult.id);
                        })
                        .catch(function(){ self.notFbLink = true; });
                    }
                } else {
                    $facebook.api("/"+ self.addPageGroup.split("/")[3]).then(function(pageInfo) {                   
                        self.groupPanel = false;
                        self.checkResult = pageInfo;
                        self.checkType = "粉絲專頁";
                        checkPageIsExist(self.checkResult.id);
                    })
                    .catch(function(){ self.notFbLink = true; });
                }
            }
        }

        function addToCrawlerList() {
            if (self.addPageGroup.split("/")[3] === "groups") {
                var newGroup = {};
                newGroup.facebookId = self.checkResult.id;
                newGroup.name = self.checkResult.name;
                newGroup.privacy = self.checkResult.privacy;
                setup.addToGroupCrawlerList(newGroup)
                .then(function() {
                    self.uploadStatus = true;
                    self.btnUpload = "成功";
                    getCrawlerList();
                })
                .catch(function(){                     
                    self.uploadStatus = false;
                    self.btnUpload = "失敗";
                });
            } else {
                var newPage = {};
                newPage.facebookId = self.checkResult.id;
                newPage.name = self.checkResult.name;
                setup.addToPageCrawlerList(newPage)
                .then(function() {
                    self.uploadStatus = true;
                    self.btnUpload = "成功";
                    getCrawlerList();
                })
                .catch(function(){                     
                    self.uploadStatus = false;
                    self.btnUpload = "失敗";
                });
            }
        }

        function previousGroup() {
            self.lastGroup = false;
            checkGroupIndex--;
            self.checkResult = checkGroupList.data[checkGroupIndex];
            checkIndex(checkGroupIndex);
            checkGroupIsExist(self.checkResult.id);
        }

        function nextGroup() {
            self.firstGroup = false;
            checkGroupIndex++;
            self.checkResult = checkGroupList.data[checkGroupIndex];
            checkIndex(checkGroupIndex);
            checkGroupIsExist(self.checkResult.id);
        }

        function changeToPageList() {
            self.items = crawlerList.message.pages;
            self.totalItems = parseInt(crawlerList.message.pages.length);
            self.atPageList = true;
        }

        function changeToGroupList() {
            self.items = crawlerList.message.groups;
            self.totalItems = parseInt(crawlerList.message.groups.length);
            self.atPageList = false;
        }

        function stopCrawl(facebookId) {
            var crawlItem = {};
            crawlItem.facebookId = facebookId;
            if (self.atPageList === true) {
                crawlItem.type = "Page";
            } else {
                crawlItem.type = "Group";
            }
            setup.stopCrawl(crawlItem)
            .then(function() { getCrawlerList(); });
        }

        function restartCrawl(facebookId) {
            var crawlItem = {};
            crawlItem.facebookId = facebookId;
            if (self.atPageList === true) {
                crawlItem.type = "Page";
            } else {
                crawlItem.type = "Group";
            }
            setup.restartCrawl(crawlItem)
            .then(function() { getCrawlerList(); });
        }

        function checkIndex(currentIndex) {
            if (currentIndex === checkGroupAmount-1) { self.lastGroup = true; } 
            else { self.lastGroup = false; }

            if (checkGroupIndex === 0) { self.firstGroup = true; } 
            else { self.firstGroup = false; }
        }

        function checkPageIsExist(facebookId) {
            var q = $q.defer();
            var i = 0;
            self.notFbLink = false;
            self.btnUpload = "";
            while (i < crawlerList.message.pages.length) {
                if (facebookId === crawlerList.message.pages[i].facebookId) {
                    self.notFbLink = true;
                    self.btnUpload = "已經存在爬蟲清單";
                    q.reject();
                    break;
                } else {
                    self.notFbLink = false;
                    self.btnUpload = "";
                    q.resolve(); 
                }
                i++;
            }
            return q.promise;
        }

        function checkGroupIsExist(facebookId) {
            var q = $q.defer();
            var j = 0;
            self.notFbLink = false;
            self.btnUpload = "";
            while (j < crawlerList.message.groups.length) {
                if (facebookId === crawlerList.message.groups[j].facebookId) {
                    self.notFbLink = true;
                    self.btnUpload = "已經存在爬蟲清單";
                    q.reject();
                    break;
                } else {
                    self.notFbLink = false;
                    self.btnUpload = ""; 
                    q.resolve(); 
                }
                j++;
            }
            return q.promise;
        }
    }
})();
