(function () {
    'use strict';

    /**
     * @ngdoc function
     * @name mostDisasterApp.controller:ListCtrl
     * @description
     * # ListCtrl
     * Controller of the mostDisasterApp
     */ 
    angular.module('mostDisasterApp')
      .controller('ListCtrl', List);
      
    List.$inject = ['$rootScope', '$localStorage', '$state', 'moment','ngDialog', 'list'];
    
    function List($rootScope, $localStorage, $state , moment, ngDialog,  list) {
        var self = this;
        var articlelist = {};
        var token = {};
        var dt1 = self.dt1 = {};
        var dt2 = self.dt2 = {};
        token.token = $localStorage.token;
        $rootScope.user = $localStorage.user;
        $rootScope.$state = $state;
        
        self.push = push;
        self.getArticlelist = getArticlelist;
        self.cancel = cancel;
        self.change;
        self.currentPage = 1;
        self.pageSize = 8;
        self.condition = [];
        self.date = [];
        self.notify = {};
        self.notify.lat='';
        self.notify.lng='';
        self.notify.message='';
        
        var state = self.state = {};
        state.earthquack = true;
        state.typhoon = true;
        state.dengue = true;
        state.winter_disaster = true;
        self.state_all = true;

        activate();
        
        function activate() {
            if (!$localStorage.user) { $state.go('login'); } // 若未登入導向到登入頁面
            self.date  = [moment(dt1.dt).set({'hour': 0,'minute': 0,'second': 0,'millisecond': 0}).valueOf(),moment(dt2.dt).set({'hour': 0,'minute': 0,'second': 0,'millisecond': 0}).valueOf()];
            getArticlelist(1);
            datechoice(dt1);
            datechoice(dt2);
            //getarticlenumber();
            self.atPageList = true; 
            self.maxSize = 7;
            token.limit = 7;
            token.page = 1;
            list.getArticlelist(token).then(function(articles) {
                articlelist = articles;
                self.items = articlelist;
            })
            .catch(function(err){ self.error = true; console.log(err); });
        } 
        
        function push(){
            console.log(self.notify);
            list.push_notification(self.notify).then(function(result){
                console.log(result);
            }).catch(function(err){ self.error = true; console.log(err); });
            ngDialog.close('push');
        }

        function getarticlenumber(){
            condition();
            token.condition = self.condition;
            token.date = self.date;
            list.getArticlenumber(token).then(function(number){
                console.log(number);
                self.totalItems = parseInt(number);
            }).catch(function(err){ self.error = true; console.log(err); });
        }
        function datechoice(dt){
            dt.today = function() {
                dt.dt = new Date();
                dt1.dt.setDate(dt.dt.getDate() - 5);
                console.log(new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate(), 0, 0, 0));
            };

            dt.today();

            dt.clear = function() {
                dt.dt = null;
            };

            dt.inlineOptions = {
                customClass: getDayClass,
                minDate: new Date(),
                showWeeks: true
            };

            dt.dateOptions = {
                //dateDisabled: disabled,
                formatYear: 'yy',
                maxDate: new Date(2020, 5, 22),
                minDate: new Date(),
                startingDay: 1
            };

            dt.toggleMin = function() {
                dt.inlineOptions.minDate = dt.inlineOptions.minDate ? null : new Date();
                dt.dateOptions.minDate = dt.inlineOptions.minDate;
            };

            dt.toggleMin();
            
            dt.open = function() {
                dt.choice.opened = true;
            };
            
            dt.choice = {
                opened: false
            };

            dt.setDate = function(year, month, day) {
                dt.dt = new Date(year, month, day);
            };

            dt.formats = ['yyyy-MM-dd', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
            dt.format = dt.formats[0];
            dt.altInputFormats = ['M!/d!/yyyy'];

            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            var afterTomorrow = new Date();
            afterTomorrow.setDate(tomorrow.getDate() + 1);
            dt.events = [{
                date: tomorrow,
                status: 'full'
            },{
                date: afterTomorrow,
                status: 'partially'
            }];

            // Disable weekend selection
            function disabled(data) {
                var date = data.date,
                mode = data.mode;
                return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
            }

            function getDayClass(data) {
                var date = data.date,
                mode = data.mode;
                if (mode === 'day') {
                    var dayToCheck = new Date(date).setHours(0,0,0,0);
                    for (var i = 0; i < dt.events.length; i++) {
                        var currentDay = new Date(dt.events[i].date).setHours(0,0,0,0);
                        if (dayToCheck === currentDay) {
                        return dt.events[i].status;
                        }
                    }
                }
                return '';
            }
        }
        
        self.check = function(startdate , enddate){
            if(moment(startdate).valueOf()>moment(enddate).valueOf()){
                alert('結束時間無法比開始時間早');
                dt2.dt = dt1.dt;
            }
        }; 

        function getArticlelist(page){
            getarticlenumber();
            var params = {};
            params.limit = 7;
            params.page = page;
            params.token = $localStorage.token;
            condition();
            params.condition = self.condition;
            params.date = self.date;
            list.getArticlelist(params).then(function(articles) {
                articlelist = articles;
                self.items = articles;
                console.log(articles);
            })
            .catch(function(err){ self.error = true; console.log(err); });
            self.atPageList = false;
            self.maxSize = 7;
        }
        
        function cancel(){
            if(!state.state_all){
                state.earthquack = false;
                state.typhoon = false;
                state.dengue = false;
                state.winter_disaster = false;
            }
        }

        function condition(){
            self.condition=[];
            if(state.earthquack){
                self.condition.push({type:"earthquack"});
            }
            if(state.typhoon){
                self.condition.push({type:"typhoon"});
            }
            if(state.dengue){
                self.condition.push({type:"dengue"});
            }
            if(state.winter_disaster){
                self.condition.push({type:'winter_disaster'});
            }   
            self.date  = [moment(dt1.dt).set({'hour': 0,'minute': 0,'second': 0,'millisecond': 0}).valueOf(),moment(dt2.dt).set({'hour': 0,'minute': 0,'second': 0,'millisecond': 0}).valueOf()];
        }


        self.overview =  function(view) {
            if(view.title===null) {
                view.title = '文章來源 Facebook';
            }
            var over = 
                '<div id="overview" class="ngdialog-message">' +
                '<h2>'+ view.title +'</h2>' +
                '<h4>'+ moment(view.created_time).format("YYYY-MM-DD h:mm:ss") +'</h4>'+
                "<div>"+ view.content +"</div><br>" +
                "<button type='button' class='btn btn-success' ng-click=\"confirm()\">推播</button><span> </span>" +
                "<button type='button' class='btn btn-danger' ng-click=\"closeThisDialog('overview')\">關閉</button>" +
                "</div>";
            self.dia = true;
            ngDialog.openConfirm({ 
                template: over,
                className: 'ngdialog-theme-default ngdialog-theme-custom',
                width: 650,
                plain: true,
                closeByEscape:true,
                closeByDocument:true,
                closeByNavigation: true
            }).then(function(value){
                console.log(view.content.replace(/[&\|\\\*^%$#@\-]/g,""));
                var push = 
                    "<div class='ngdialog-message' id='push'>" +
                    "<p>推播任務</p>"+
                    "<div class='input-group'>"+
                    "<span class='input-group-addon' id='basic-addon'>經度：</span>"+
                    "<input type='text' id='lat' class='form-control' placeholder='121' aria-describedby='basic-addon' ng-model='list.notify.lat'>"+
                    "<span class='input-group-addon' id='basic-addon1'>緯度：</span>"+
                    "<input type='text' id='lng' class='form-control' placeholder='23.5' aria-describedby='basic-addon1' ng-model='list.notify.lng'>"+
                    "</div></br>"+
                    "<div class='input-group'>"+
                    "<span class='input-group-addon' id='basic-addon1'>推播<br>訊息</span>"+
                    "<textarea id='message' class='form-control' rows='5' id='comment'>"+view.content+"</textarea>"+
                    "</div></br>"+
                    "<button class='btn-success btn' onclick='push()' ng-click='closeThisDialog()'>推播任務</button><span> </span>"+
                    "<button class='btn-danger btn' ng-click='closeThisDialog()'>取消</button>"+
                    "</div>";
                ngDialog.close('overview');
                ngDialog.openConfirm({ template: push,
                    className: 'ngdialog-theme-default ngdialog-theme-custom',
                    width: 650,
                    plain: true,
                    closeByEscape:true,
                    closeByDocument:true,
                    closeByNavigation: true
                });
            });
        };
    }
})();

function push(){
    var lat = document.getElementById('lat').innerHTML;
    var lng = document.getElementById('lng').innerHTML;
    var message = document.getElementById('message').innerHTML;
    console.log(lat+' '+lng+' '+message);
    $.ajax({
        type: 'POST',
        url: 'http://140.118.109.149:3002/map/push',//?lat='+parseInt(lat)+'&lng='+parseInt(lng)+'&message='+message,
        crossDomain: true,
        data:JSON.stringify({lat:parseInt(lat),lng:parseInt(lng),message:message}),
        headers: {'Content-Type': 'application/json; charset=utf-8'},
        dataType: "json",
        success: function (err) {
            if (err) {
                console.log(err);
            } else {
                console.log('success');
            }
        }
    });
}