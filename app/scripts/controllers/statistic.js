(function () {
  'use strict';

  /**
   * @ngdoc function
   * @name mostDisasterApp.controller:StatisticCtrl
   * @description
   * # StatisticCtrl
   * Controller of the mostDisasterApp
   */

  angular.module('mostDisasterApp')
    .controller('StatisticCtrl', Statistic);

    Statistic.$inject = ['$rootScope', '$localStorage', '$state', 'statistic'];

    function Statistic($rootScope, $localStorage, $state, statistic) {
      var self = this;
      var token = {};
      token.token = $localStorage.token;
      $rootScope.user = $localStorage.user;
      $rootScope.$state = $state;
      
      self.getArticle = getArticle; // 取得文章
      self.getStatistic = getStatistic; // 取得文章
      self.getDonut = getDonut;
      self.data = [[1 , 0, 0],[2 , 5, 0],[387 , 0, 0],[2 , 5, 0],[14 , 12, 123],[23 , 31, 20]];
      self.kind='';
      self.labels = ["flooding", "typhoon", "earthquack"];
      self.donuts = [];
      self.con = ['1%','2%',['32%','33%'],'4%','7%',['80%','85%']];
      activate();

      function activate() {
        if (!$localStorage.user) { $state.go('login'); } // 若未登入導向到登入頁面
        //getArticle();
        getDonut();
        getStatistic(null);
      }

      function getDonut(){
          statistic.getDonut(token).then(function(donut) {
                self.donuts = donut;
                console.log(donut);
                var chart = c3.generate({
                  size: {
                      height: 600,
                      width: 600
                  },  
                  data: {
                      columns: donut,
                      type : 'donut',
                      onclick: function (d, i) { console.log(d, i); 
                        //getStatistic(d.id);  
                      },
                      onmouseover: function (d, i) { console.log(d.id);
                        self.kind=d.id;
                        getStatistic(d.id); 
                      },
                      onmouseout: function (d, i) { console.log(d.id);
                        self.kind='';
                        getStatistic(null); 
                      }
                  },
                  color:{pattern:['#06aed5','#086788','#9fffcb','#004e64','#25a18e']}
                });
          }).catch(function(err){ self.error = true; console.log(err); });
      }
      function getArticle(params) {
        token.params = params;
        statistic.getArticlelist(token).then(function(articles) {
                self.items = articles;
            }).catch(function(err){ self.error = true; console.log(err); });
      }

      function getStatistic(params) {
        token.params = params;
        statistic.getStatistic(token).then(function(statistic_data) {
          self.items = statistic_data;
        }).catch(function(err){ self.error = true; console.log(err); });
      }
    }
})();