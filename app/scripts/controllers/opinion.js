(function () {
  'use strict';

  /**
   * @ngdoc function
   * @name mostDisasterApp.controller:OpinionCtrl
   * @description
   * # OpinionCtrl
   * Controller of the mostDisasterApp
   */
  angular.module('mostDisasterApp')
    .controller('OpinionCtrl', Opinion);

    Opinion.$inject = ['$rootScope', '$localStorage', '$state', '$q', 'moment' , 'opinion'];

    function Opinion($rootScope, $localStorage, $state , Q, moment, opinion) {
      var self = this;
      var dt1 = self.dt1 = {};
      var dt2 = self.dt2 = {};
      //self.getArticle = getArticle; // 取得文章
      
      self.report = report;
      self.cancel = cancel;

      self.show = false;
      self.condition = [];
      
      var state = self.state = {};
      state.earthquack = true;
      state.typhoon = true;
      state.dengue = true;
      state.winter_disaster = true;
      self.state_all = true;

      $rootScope.user = $localStorage.user;
      $rootScope.$state = $state;
      
      activate();
      
      function activate() {
          if (!$localStorage.user) { $state.go('login'); } // 若未登入導向到登入頁面
          //getArticle();
          self.max = 200;
          datechoice(self.dt1);
          datechoice(self.dt2);
      }
      
       function datechoice(dt){
            dt.today = function() {
                dt.dt = new Date();
            };
            dt.today();

            dt.clear = function() {
                dt.dt = null;
            };

            dt.inlineOptions = {
                customClass: getDayClass,
                minDate: new Date(),
                showWeeks: true
            };

            dt.dateOptions = {
                //dateDisabled: disabled,
                formatYear: 'yy',
                maxDate: new Date(2020, 5, 22),
                minDate: new Date(),
                startingDay: 1
            };

            dt.toggleMin = function() {
                dt.inlineOptions.minDate = dt.inlineOptions.minDate ? null : new Date();
                dt.dateOptions.minDate = dt.inlineOptions.minDate;
            };

            dt.toggleMin();

            dt.open = function() {
                dt.choice.opened = true;
            };
            
            dt.choice = {
                opened: false
            };

            
            dt.setDate = function(year, month, day) {
                //console.log(year + month + day);
                dt.dt = new Date(year, month, day);
            };

            dt.altInputFormats = ['yyyy-MM-dd'];

            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            var afterTomorrow = new Date();
            afterTomorrow.setDate(tomorrow.getDate() + 1);
            dt.events = [
                {
                date: tomorrow,
                status: 'full'
                },
                {
                date: afterTomorrow,
                status: 'partially'
                }
            ];
            
            // Disable weekend selection
            function disabled(data) {
                var date = data.date,
                mode = data.mode;
                return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
            }
            
            function getDayClass(data) {
                var date = data.date,
                mode = data.mode;
                    if (mode === 'day') {
                    var dayToCheck = new Date(date).setHours(0,0,0,0);
                    for (var i = 0; i < dt.events.length; i++) {
                        var currentDay = new Date(dt.events[i].date).setHours(0,0,0,0);
                        if (dayToCheck === currentDay) {
                            return dt.events[i].status;
                        }
                    }
                }
                return '';
            }
        }
        
        self.check = function(startdate , enddate){
            if(moment(startdate).valueOf()>moment(enddate).valueOf()){
                alert('結束時間無法比開始時間早');
                dt2.dt = dt1.dt;
            }
        }; 

        self.select_change = function(state_all){
            if(state_all){
                state.earthquack = true;
                state.typhoon = true;
                state.dengue = true;
                state.winter_disaster = true;
            }else{
                state.earthquack = false;
                state.typhoon = false;
                state.dengue = false;
                state.winter_disaster = false;   
            }
        }; 

      //progress bar
      function randomStacked(positive,negative) {
          self.stacked = [];
          
          var total = positive + negative;
          var types = ['success','danger'];

          var index = Math.floor(Math.random() * 4);
          self.stacked.push({
            value: Math.floor( positive / total * 100 ),
            type: types[0]
          });
          self.stacked.push({
            value:100- Math.floor( ( positive / total ) * 100 ),
            type: types[1]
          });    
      }
      function cancel(){
            if(!state.state_all){
                state.earthquack = false;
                state.typhoon = false;
                state.dengue = false;
                state.winter_disaster = false;
            }
        }

        function condition(){
            self.condition=[];
            if(state.earthquack){
                self.condition.push({class:"earthquack"});
                //console.log(state.earthquack);
            }if(state.typhoon){
                self.condition.push({class:"typhoon"});
                //console.log(state.typhoon);
            }if(state.dengue){  
                self.condition.push({class:"dengue"});
                //console.log(state.dengue);
            }if(state.winter_disaster){
                self.condition.push({class:"winter_disaster"});
                //console.log(state.winter_disaster);
            }
            //console.log(self.condition);
        }
      function report(){
          var oo = {};
          oo.start = moment(dt1.dt).format('l').valueOf();
          oo.end = moment(dt2.dt).format('l').valueOf();
          console.log(oo.start+ ' ' +oo.end);
          condition();
          oo.condition = self.condition;
          var analyze=[];
          
          opinion.getopinion(oo).then(function(result){
              analyze=result;
              console.log(result); 
              statsic(analyze[0]);
              //callback(result)
          }).catch(function(err){ self.error = true; console.log(err); });
 
        function statsic(analyze){
            randomStacked(sum(analyze.positive),sum(analyze.negetive));
            function sum(x) {
              var i,opiinioSum=0;
              for(i in x){
                  if(i!==0) {
                    opiinioSum = opiinioSum +x[i];
                  }
              }
              return opiinioSum;
            }

            self.show = true;
            var t = analyze.date;
            var p = analyze.positive;
            var n = analyze.negetive;

            c3.generate({
                bindto: '#chart',
                data: {
                    x:'x',
                    xFormat: '%m/%d/%Y',
                    //xFormat: '%Y-%m-%d',
                    columns: [t,p,n],
                    colors:{positive:'#5bb95c',negetive:'#d9534f'}
                },
                axis: {
                    x: {
                        type: 'timeseries',
                        tick: {format: '%Y-%m-%d'}
                    }
                }
            });
        }
    }
      
    }
})();