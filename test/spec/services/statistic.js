'use strict';

describe('Service: statistic', function () {

  // load the service's module
  beforeEach(module('mostDisasterApp'));

  // instantiate service
  var statistic;
  beforeEach(inject(function (_statistic_) {
    statistic = _statistic_;
  }));

  it('should do something', function () {
    expect(!!statistic).toBe(true);
  });

});
